import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import { changeName } from "../../store/modules/user/actions";

const UserCard = () => {
    const [newName, setNewName] = useState()

    const dispatch = useDispatch()

    const handleClick = () => dispatch(changeName(newName))

    const name = useSelector(state => state.user)

    return (
        <div className="user_card">
            <p key={name}>User name: {name.name}</p>
            <input type="text" onChange={e => setNewName(e.target.value)} />
            <button onClick={handleClick}>Change</button>
        </div>
    )
}

export default UserCard